# gator
gator is used for running unit tests against gatekeeper policies. See their documentation at https://open-policy-agent.github.io/gatekeeper/website/docs/gator/

The gator CLI is a tool for evaluating Gatekeeper ConstraintTemplates and Constraints in a local environment. [More Information](https://open-policy-agent.github.io/gatekeeper/website/docs/gator/)

This container is used to run automated pipeline checks against your custom contstraints.

# How to use
Add the following into your .gitlab-ci.yml.
```
gator-test:
  stage: test
  tags:
    - amd64
  image: akozak1lab/gator:latest
  script:
  - gator test <path to suite file>/suite.yaml
```

## Suit files are documented here
Information about [Suites](https://open-policy-agent.github.io/gatekeeper/website/docs/gator/#suites)

[Example Suite File](https://github.com/open-policy-agent/gatekeeper-library/blob/8765ec11c12a523688ed77485c7a458df84266d6/library/general/allowedrepos/suite.yaml)
